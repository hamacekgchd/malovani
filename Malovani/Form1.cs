﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Malovani
{
    public partial class Form1 : Form
    {
        Bitmap obrazek;
        Graphics g;

        public Form1()
        {
            InitializeComponent();
            obrazek = new Bitmap(platno.Width, platno.Height);
            platno.Image = obrazek;
            g = Graphics.FromImage(obrazek);
        }

        private void platno_MouseUp(object sender, MouseEventArgs e)
        {
            Rectangle mouse = new Rectangle(e.X - 5, e.Y - 5, 10, 10);
            Brush b = new SolidBrush(Color.FromArgb(0, 255, 0));
            g.FillEllipse(b, mouse);
            platno.Refresh();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            SaveFileDialog dialog = (SaveFileDialog)sender;
            string filename = dialog.FileName;
            obrazek.Save(filename);
        }

        private void uložitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void platno_MouseMove(object sender, MouseEventArgs e)
        {
            
        }

        private void platno_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawLine(Pens.Black, 0, 0, DateTime.Now.Millisecond, 100);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            platno.Refresh();
        }
    }
}
